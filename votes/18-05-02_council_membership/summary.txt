=====================================================================
Summary
=====================================================================

Topic: Community Council Membership
Date taken: 17/4/16 - 18/5/2
Vote count: 48 (of 96, 50%)

Secretary: Karsten Loesing

Topic put to a vote were...

  Question 1: List up to five of these individuals you'd like to see
  on the new Community Council.

  * David Goulet (dgoulet)
  * George Rosamond (gman999)
  * Gus (ggus)
  * Juris (juris)
  * Silvia Puglisi (hiro)
  * Taylor Yu (catalyst)

  Question 2: Would you be uncomfortable having any of these people
  adjudicate your issues?

Results were...

  * The next council will be composed of David, George, Gus, Silvia, and
    Taylor.

=====================================================================
Votes
=====================================================================

Alexander Færøy
Alison Macrina
Allen Gunn
Antonela Debiasi
Arthur D. Edelstein
Arturo Filastò
chelsea komlo
Colin Childs
Damian Johnson
David Goulet
Georg Koppen
George Kadianakis
george
gus
hiro
Ian Goldberg
isabela
isis
Jens Kubieziel
Julius Mittenzwei
juris
kat
Kathleen Brade
Linus Nordberg
Mark Smith
Matthew Finkel
micah
Micah Lee
Nick Mathewson
Nicolas Vigier
Nima Fatemi
Parinishtha Yadav
Paul Syverson
Philipp Winter
Rabbi Rob Thomas
Richard Pospesel
Roger Dingledine
S. Banerian
Sebastian Hahn
Stephanie A. Whited
Steven Murdoch
Sukhbir Singh
Taylor Yu
teor
Tom Ritter
Tommy Collison
Vasilis
Yawning Angel

1412;dgoulet,gman999,juris,hiro,catalyst;<abstain>
1713;juris,hiro,catalyst;<abstain>
2149;dgoulet,gman999,juris;ggus,catalyst
2372;dgoulet,gman999,ggus,hiro,catalyst;juris
2434;dgoulet,juris,hiro;<abstain>
2871;dgoulet,gman999,hiro,catalyst;juris
3162;dgoulet,gman999,juris,hiro;<abstain>
3865;dgoulet,ggus,hiro;<none>
4288;gman999,hiro;<abstain>
7734;dgoulet,ggus,juris,hiro;<abstain>
8522;dgoulet,ggus,hiro,catalyst;juris
8836;dgoulet,gman999,ggus,hiro;catalyst
9019;dgoulet,gman999,hiro,catalyst;<abstain>
9893;dgoulet,gman999,juris;ggus,hiro,catalyst
10034;<abstain>;<abstain>
10628;dgoulet,hiro;catalyst
10671;dgoulet,gman999,ggus,hiro;<abstain>
10715;ggus,hiro,catalyst;<abstain>
10863;<abstain>;<abstain>
11021;dgoulet,gman999,ggus,juris,hiro;<abstain>
11794;ggus,juris,hiro,catalyst;<abstain>
11993;dgoulet,gman999,ggus,hiro,catalyst;juris
12683;<abstain>;juris
13400;dgoulet,gman999,hiro,catalyst;<abstain>
13870;dgoulet,hiro,catalyst;<abstain>
14713;dgoulet,hiro,catalyst;juris
16888;dgoulet,gman999,ggus,hiro,catalyst;<abstain>
17557;hiro,catalyst,dgoulet,ggus,gman999;<abstain>
17906;dgoulet,hiro,catalyst;juris
21292;hiro,dgoulet;<abstain>
21386;dgoulet,gman999,ggus,juris,hiro;<abstain>
21862;dgoulet,ggus,hiro,catalyst;juris
22010;dgoulet,gman999,ggus,hiro,catalyst;<abstain>
22399;dgoulet,juris,hiro;<abstain>
22453;dgoulet,gman999,ggus,juris,hiro;<abstain>
23113;dgoulet,gman999,ggus,hiro,catalyst;juris
23924;dgoulet,gman999,ggus,hiro;<abstain>
24360;dgoulet,gman999;<abstain>
25072;dgoulet,gman999,ggus,juris,hiro;catalyst
25158;hiro,catalyst;<abstain>
25180;dgoulet,gman999,ggus,hiro;<abstain>
25876;dgoulet,hiro,catalyst;<abstain>
27396;gman999,ggus,juris,hiro,catalyst;dgoulet
27943;dgoulet,juris,hiro;catalyst,george
30151;dgoulet,gman999,ggus,hiro,catalyst;<abstain>
31618;gman999,hiro;<abstain>
31819;dgoulet,gman999,ggus,juris,hiro;catalyst
32262;dgoulet,gman999,ggus,hiro,catalyst;<abstain>

=====================================================================
Question 1: List up to five of these individuals you'd like to see
on the new Community Council.
=====================================================================

dgoulet: 38 (79%)
gman999: 28 (58%)
ggus: 24 (50%)
juris: 16 (33%)
hiro: 42 (88%)
catalyst: 23 (48%)

---------------------------------------------------------------------
Result:
---------------------------------------------------------------------

The next council will be composed of David, George, Gus, Silvia, and
Taylor.

=====================================================================
Question 2: Would you be uncomfortable having any of these people
adjudicate your issues?
=====================================================================

dgoulet: 1 (2%)
gman999: 1 (2%)
ggus: 2 (4%)
juris: 9 (19%)
hiro: 1 (2%)
catalyst: 7 (15%)

---------------------------------------------------------------------
Result:
---------------------------------------------------------------------

Nobody reached 25% so everyone's eligible to be on the council.

